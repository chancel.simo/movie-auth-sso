import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArticleComponent } from './page/article/article.component';
import { EditArticleComponent } from './page/article/edit-article.component';
import { LoginComponent } from './page/authentication/login/login.component';
import { LogoutComponent } from './page/authentication/logout.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'article', component: ArticleComponent },
  { path: 'article/edit', component : EditArticleComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'lazy', loadChildren: './lazy/lazy.module#LazyModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
