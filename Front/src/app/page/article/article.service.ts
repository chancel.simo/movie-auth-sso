import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

export interface articleDto{
    id?:string,
    titre?:string,
    contenu?:string,
}

@Injectable({
    providedIn : 'root',
})
export class ArticleService{

    url = "http://localhost:3000/article/";
    constructor(
        private http: HttpClient
    ){}

    async getArticles(){
        let data : any;
        await this.http.get(this.url).toPromise().then( r => {
            data = r
        });
        return data;
    }

    async getArticle(id: string = undefined ){
        let data : any;
        await this.http.get(`${this.url + id}`).toPromise().then( r => {
            data = r
        });
        return data;
    }

    async addArticle(articleDto: articleDto){
        return await this.http.post(this.url, articleDto).subscribe(
            () => {
              console.log('Enregistrement terminé !');
            },
            (error) => {
              console.log('Erreur ! : ' + error);
            }
          );
    }
    
}

