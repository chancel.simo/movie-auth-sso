import { ApiPropertyOptional } from "@nestjs/swagger";

export class userRoleDto{
    @ApiPropertyOptional()
    role_name: string;
}