import { userRoleDto } from './user_role.dto';
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { UserRoleEntity } from "./user_role.entity";


@Injectable()

export class UserRoleService{
    constructor(
        @InjectRepository(UserRoleEntity)
        private userRoleRepository: Repository<UserRoleEntity>
    ){}

    async getRole(roleDto : userRoleDto){
       return await this.userRoleRepository.findOne({ where : { role_name : roleDto.role_name } });
    }
    async addRole(roleDto : userRoleDto){
        return await this.userRoleRepository.save(roleDto);
    }

}
