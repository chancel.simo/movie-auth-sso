import { SetMetadata } from "@nestjs/common";
import { ApiPropertyOptional } from "@nestjs/swagger";
import { Column, Entity } from "typeorm";
import { BaseEntity } from "../baseFiles/baseEntity";


export enum userRole{
    Admin = 'admin',
    User = 'user',
}

export const ROLES_KEY = 'roles';
export const Roles = (...roles: userRole[]) => SetMetadata(ROLES_KEY, roles);

@Entity('user_role')

export class UserRoleEntity extends BaseEntity{
    @Column('varchar')
    role_name: string;
}