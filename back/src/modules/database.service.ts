import { GetUserRequest, GetUserResponse } from './user/user.dto';
import { UsersService } from './user/user.service';

import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { userRoleDto } from "./user_roles/user_role.dto";
import { userRole, UserRoleEntity } from "./user_roles/user_role.entity";
import { UserRoleService } from "./user_roles/user_role.service";

@Injectable()

export class DataBaseService{
    constructor(
       private userRoleService : UserRoleService,
       private usersService : UsersService,
    )
    { }

    async seedDb(){
        await this.createUserRole();
        await this.createAdminUser();
    }
    private async createUserRole(){
        for(const role of Object.values(userRole)){
            const roleResponse = await this.userRoleService.getRole({ role_name : role });
            if(!roleResponse){
                const roleDto = new userRoleDto();
                roleDto.role_name = role;
                const response = await this.userRoleService.addRole(roleDto);
            }
        }
    }

    private async createAdminUser(){
        
        const user:GetUserRequest = { 
            login : "admin",
            password : "admin",
            lastName : "test", 
            role : {
                role_name : userRole.Admin
            }
        };

        const userResponse = await this.usersService.findAll();
        const index = userResponse.findIndex(x => x.login === user.login);
        if(index === -1){
            await this.usersService.createOrUpdate(user);
        }
    }
}