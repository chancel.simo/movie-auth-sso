
import { userRole } from './../user_roles/user_role.entity';
import { UserEntity } from './user.entity';
import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from 'typeorm';
import { GetUserResponse, UserDto, GetUserRequest, getAuthRequest } from './user.dto';
import * as bcrypt from 'bcrypt';
@Injectable()
export class UsersService{
    constructor(
        @InjectRepository(UserEntity)
        private usersRepository: Repository<UserEntity>
    ){}

    findAll():Promise<GetUserResponse[]>{
        return this.usersRepository.find({ relations : ['role'] });
    }

    findOne(id:string):Promise<GetUserResponse>{
        return this.usersRepository.findOne(id);
    }
    async createOrUpdate(user:GetUserRequest){
        if(user){
        user.password = await bcrypt.hashSync(user.password, 10);
        console.log(user);
            user.role = {
                role_name : userRole.User
            }
        }
        
        this.usersRepository.save(user);
    }

    delete(id:string){
        this.usersRepository.delete(id);
    }

    async login(credential: getAuthRequest){
        const { login, password } = credential;

        const user = await this.usersRepository.createQueryBuilder("user").
                        where("user.login = :login or user.password = :login",
                              { login }).getOne();
        if(!user){
            throw new NotFoundException('Erreur login'); 
        }

        const hashedPass = await bcrypt.compare(password, user.password);

        if(hashedPass){
            console.log('user success');
            return user;
        }else{
            throw new NotFoundException('Erreur Password'); 
        }
    }
}