import { UsersService } from './user.service';
import { Body, Controller, Delete, Get, Param, Post, Req, } from "@nestjs/common";
import { GetUserRequest, UserDto, GetUserResponse, getAuthRequest } from './user.dto';

@Controller('users')

export class UserController{
    constructor(
        private usersService:UsersService
    ){}

    @Get()
    async getAllUsers():Promise<GetUserResponse[]>{
        return await this.usersService.findAll();
    }

    @Get(':id')
    async getUser(@Param('id') id:string){
        console.log("looking for");
        return await this.usersService.findOne(id);
    }
    
    
    @Post('/auth/login')
    async login(@Body() req : getAuthRequest){
        console.log(req)
        return await this.usersService.login(req);
    }

    @Post()
    async createOrUpdate(@Body() user:GetUserRequest){
        console.log(user)
        await this.usersService.createOrUpdate(user);
        
        
    }

    @Delete()
    deleteUser(@Req() id:string){
        this.usersService.delete(id);
    }

}