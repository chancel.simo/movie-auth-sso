import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Article } from "./article.entity";

@Injectable()
export class ArticleService{

    constructor(
        @InjectRepository(Article)
        private articleRepository: Repository<Article>
    ){}
    
    async getArticles(){
        return await this.articleRepository.find();
    }

    async getArticle(id: string){
        return await this.articleRepository.findOne(id);
    }

    async addArticle(articleDto: { titre:string, contenu:string, userId:string }){
        return await this.articleRepository.save(articleDto);
    }

}