import { UsersRoleModule } from './modules/user_roles/user_role.module';
import { UsersModule } from './modules/user/user.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as dotenv from 'dotenv';
import { DataBaseService } from './modules/database.service';
import { UserRoleService } from './modules/user_roles/user_role.service';
import { ArticlesModule } from './modules/article/article.module';

dotenv.config();

@Module({
  imports: [
    UsersModule,
    UsersRoleModule,
    ArticlesModule,
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DATABASE_HOST,
      port: parseInt(process.env.DATABASE_PORT),
      username: process.env.DATABASE_USERNAME,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE_NAME,
      entities: ["dist/**/**/*.entity{.ts,.js}"],
      synchronize: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService, DataBaseService],
})
export class AppModule {
  constructor(
    private dbService: DataBaseService,
  ){
    this.init()
  }
  private async init(){
    this.dbService.seedDb();
  }
}
